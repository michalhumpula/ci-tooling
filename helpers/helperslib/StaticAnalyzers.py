import subprocess
import multiprocessing
import threading
import queue
import os
import json
import functools
import tempfile
import re
import glob
import hashlib
from os.path import abspath

from itertools import groupby
from xml.sax.saxutils import escape


def resolveBaseCommitForBranch(sourceDir, targetBranch):
	depth = 50

	cmd = [ 'git', 'fetch', '--depth=' + str(depth), 'origin', targetBranch ]
	subprocess.check_call(cmd, cwd=sourceDir)

	cmd = [ 'git', 'merge-base', 'HEAD', 'origin/{}'.format(targetBranch) ]
	result = subprocess.run(cmd, capture_output=True, cwd=sourceDir, encoding='utf-8')
	try:
		result.check_returncode()
	except subprocess.CalledProcessError as e:
		print("Error: {}".format(e.stdout), e.stderr)
		raise

	baseCommit = result.stdout.strip()
	if not baseCommit:
		raise RuntimeError('Failed to find base commit for HEAD and ' + targetBranch)

	return baseCommit


def getFilesToCheck(sourceDir, baseCommit):
	extensions = ('.cpp', '.cxx', '.cc', '.c++')

	if not baseCommit:
		files = []
		for ext in extensions:
			files += filter(lambda f: not f.startswith(sourceDir + '/build/'),
				glob.glob(sourceDir + '/**/*{}'.format(ext), recursive=True))
	else:
		cmd = [ 'git', 'diff-tree', '--name-only', '--diff-filter=d', '-r', baseCommit, 'HEAD' ]
		result = subprocess.run(cmd, capture_output=True, cwd=sourceDir, encoding='utf-8')
		result.check_returncode()
		files = result.stdout.split('\n')

	return list(filter(lambda f: f.endswith(extensions), files))


def removeIgnoredFiles(sourceDir, files):
	if os.path.exists(os.path.join(sourceDir, '.clang-tidy-ignore')):
		with open(os.path.join(sourceDir, '.clang-tidy-ignore')) as infile:
			ignoreRules = filter(lambda line: line and not line.startswith('#'), map(lambda line: line.strip(), infile))
			ignoreFilter = re.compile(r'|'.join(ignoreRules))
		return list(filter(lambda f: not ignoreFilter.match(f[len(sourceDir) + 1:]), files))
	else:
		return files


def _run_analyzers(files_queue, sourceDir, buildDir, analyzers, logfile, lock):
	while True:
		name = files_queue.get()

		for analyzer in analyzers:
			with lock:
				print('Analyzing {} [{}]'.format(name, analyzer.name))
			analyzer.analyze(name, sourceDir, buildDir, logfile, lock)

		files_queue.task_done()


def _writeTestReport(sourceDir, logfile, excluded_path_prefixes):
	# error lines end with [foo-bar-error-type]
	new_error_line_re = re.compile(r'\[[\w\-,\.]+\]$')
	# /file/path:row:column error message [error-identifier]
	error_line_re = re.compile(r'^([\w\/\.\-\ ]+):(\d+):(\d+): (.+) (\[[\w\-\,\.]+\])$')
	# ignore 'N warnings generated.' outputs from clang-tidy
	clang_tidy_warnings_re = re.compile(r'\d+ warning(|s) generated')

	class StaticAnalysisError:
		def __init__(self, file_name, line, column, error_msg, error_type, description):
			self.full_file_name = abspath(file_name)
			self.file_name = self.full_file_name.replace(sourceDir, "")
			self.line = line
			self.column = column
			self.error_msg = error_msg
			self.error_type = error_type
			self.description = description

		def __eq__(self, other):
			return self.file_name == other.file_name \
				and self.line == other.line \
				and self.column == other.column \
				and self.error_msg == other.error_msg \
				and self.error_type == other.error_type \
				and self.description == other.description

		def __hash__(self):
			return hash((self.file_name, self.line, self.column, self.error_msg, self.error_type, self.description))

		def toJson(self):
			return {
					"description": "{error_type} {error_msg}".format(error_type=self.error_type, error_msg=self.error_msg),
					"fingerprint": self.lineFingerprint(),
					"location": {
						"path": self.file_name,
						"lines": {
							"begin": self.line
						}
					}
				}

		def lineFingerprint(self):
			digest = hashlib.md5()
			digest.update("{file}:{line}:{column} [{error_type}] {error_msg}".format(file=self.file_name, line=self.line, column=self.column, error_type=self.error_type, error_msg=self.error_msg).encode())
			return digest.hexdigest()

	def _isPathExcluded(path, excluded):
		for prefix in excluded:
			if path.startswith(prefix):
				return True
		return False

	def _isNewErrorLine(line):
		return line and line.startswith('/') and new_error_line_re.search(line, re.M)

	def _parseError(error_lines):
		if not error_lines:
			return None

		m = error_line_re.match(error_lines[0])
		if not m:
			print("Line {line} doesn't match!".format(line=error_lines[0]))
			return None

		return StaticAnalysisError(file_name=m.group(1), line=int(m.group(2)), column=int(m.group(3)), error_msg=m.group(4), error_type=m.group(5), description="\n".join(error_lines[1:]))

	logfile.seek(0)
	errors = []
	current_error = []
	for line in logfile:
		line = line.strip()
		if _isNewErrorLine(line):
			if current_error:
				parsed = _parseError(current_error)
				if not _isPathExcluded(parsed.full_file_name, excluded_path_prefixes):
					errors.append(parsed)
			current_error = [line]
		elif current_error and line and not clang_tidy_warnings_re.match(line) and line.strip():
			current_error.append(line)

	if current_error:
		parsed = _parseError(current_error)
		if not _isPathExcluded(parsed.full_file_name, excluded_path_prefixes):
			errors.append(parsed)

	warnCount = 0
	errorCount = 0

	reports = []
	for file, file_errors in groupby(sorted(errors, key=lambda x: x.file_name), key=lambda x: x.file_name):
		file_errors = [*{*file_errors}]
		for error in file_errors:
			reports.append(error.toJson())
			if "error:" in error.error_msg:
				errorCount += 1
			elif "warning:" in error.error_msg:
				warnCount += 1

	with open('codequality.json', 'w', encoding='utf-8') as outfile:
	    json.dump(reports, outfile, indent=4)

	return (warnCount, errorCount)

def run(sourceDir, buildDir, analyzers, filesToCheck):
	max_tasks = multiprocessing.cpu_count()

	#
	# A workaround is needed for clang-tidy trying to analyse system headers, which are obviously not part of the project to scan.
	# Heuristic is that any error or warning generated about a path that matches some system header (prefix) should be disregarded.
	#
	excluded_path_prefixes = set([])
	sys_header_include_re = re.compile(r'-isystem (\S+) ')

	def _parseCommand(line):
		sys_headers = set([])
		found = sys_header_include_re.search(line)
		while found:
			sys_headers.add(found.group(1))
			found = sys_header_include_re.search(line, found.end(1))
		return sys_headers

	with open(os.path.join(buildDir, 'compile_commands.json')) as commands_json:
		for entry in json.load(commands_json):
			excluded_path_prefixes.update(_parseCommand(entry['command']))

	with open('static-analyzer.log', 'w+', encoding='utf-8') as logfile:
		try:
			files_queue = queue.Queue(max_tasks)
			lock = threading.Lock()

			for _ in range(max_tasks):
				t = threading.Thread(target=_run_analyzers,
						     args=(files_queue, sourceDir, buildDir, analyzers, logfile, lock))
				t.daemon = True
				t.start()

			for f in filesToCheck:
				files_queue.put(f)

			files_queue.join()

		except KeyboardInterrupt:
			os.kill(0, 9)

	with open('static-analyzer.log', 'r', encoding='utf-8') as logfile:
		print("Writing test report")
		(warnCount, errCount) = _writeTestReport(sourceDir, logfile, excluded_path_prefixes)

	print("Done, found {} warnings and {} errors".format(warnCount, errCount))
	return warnCount == 0 and errCount == 0

class StaticAnalyzer:

	@staticmethod
	def _run(command, sourceDir, logfile, lock):
		r = subprocess.run(command, cwd=sourceDir, capture_output=True, encoding='utf-8')
		with lock:
			for out in (r.stdout.strip(), r.stderr.strip()):
				if out:
					logfile.write(out + '\n')
			logfile.flush()

class Clazy(StaticAnalyzer):
	name = 'clazy'

	@staticmethod
	def analyze(fileName, sourceDir, buildDir, logfile, lock):
		# TODO: load checks from non-standard .clazy file in repo root?
		cmd = [ 'clazy-standalone', '-p=' + buildDir, fileName ]

		StaticAnalyzer._run(cmd, sourceDir, logfile, lock)

class ClangTidy(StaticAnalyzer):
	name = 'clang-tidy'

	@staticmethod
	def analyze(fileName, sourceDir, buildDir, logfile, lock):
		cmd = [ 'clang-tidy', '--quiet', '-p=' + buildDir, fileName ]

		StaticAnalyzer._run(cmd, sourceDir, logfile, lock)


def analyzerForName(name):
	analyzers = {
		'clazy': Clazy,
		'clang-tidy': ClangTidy
	}

	if not name in analyzers:
	    raise RuntimeError("Unknown analyzer {}. Supported analyzers are {}".format(name, ','.join(analyzers.keys())))

	return analyzers[name]

